package com.company;

import com.github.sarxos.webcam.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;

public class CamServer {
    private final Webcam webcam = Webcam.getDefault();
    private WebcamPanel panel;
    private JFrame window;

    public void start(int port, String ip) throws Exception {
            ServerSocket s = new ServerSocket(port, 8, Inet4Address.getByName(ip));
            s.setReuseAddress(true);

            byte[] receiveData = new byte[4];
            DatagramPacket recPacket = new DatagramPacket(receiveData, receiveData.length);
            //List<ClientHandler> handlerList = new LinkedList<ClientHandler>();
            while(true) {
                ClientHandler handler = new ClientHandler(s.accept());
                //handlerList.add(handler);
                handler.start();
                System.out.println("Thread started.");
            }
    }
    public void init(){
        webcam.setViewSize(new Dimension(176,144));
        //webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open(true);
        panel = new WebcamPanel(webcam);
        panel.setFPSDisplayed(true);
        panel.setDisplayDebugInfo(true);
        panel.setImageSizeDisplayed(true);
        //panel.setMirrored(true);
        window = new JFrame("UDP");
        window.add(panel);
        window.setResizable(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    private class ClientHandler extends Thread{
        private Socket client = null;
        private InetAddress addr = null;
        private int port = -1;
        public ClientHandler(Socket client){
            this.client = client;
            this.addr = client.getInetAddress();
            this.port = client.getLocalPort();
        }
        @Override
        public void run() {
            super.run();
            try {
                DatagramSocket sender = new DatagramSocket();
                byte[] sendData;
                DatagramPacket sendPacket;

                ByteArrayOutputStream baos;
                BufferedImage img;

                InputStream in = client.getInputStream();
                while (in.available() == 0) {
                    img = webcam.getImage();
                    baos = new ByteArrayOutputStream();
                    ImageIO.write(img, "jpg", baos);
                    sendData = baos.toByteArray();
                    sendPacket = new DatagramPacket(sendData, sendData.length, addr, port);
                    sender.send(sendPacket);
                    //System.out.println("Packet sent to " + sendPacket.getAddress().toString() + ":" + sendPacket.getPort());
                    Thread.sleep(50);
                }
                System.out.println("Thread stopped.");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}

class LauncherUDP{
    public static void main(String[] args) throws Exception {
        CamServer camServer = new CamServer();
        camServer.init();
        if(args.length > 1) {
            camServer.start(Integer.parseInt(args[0]), args[1]);
        } else{
            camServer.start(20000, "127.0.0.1");
        }
    }
}