package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;

public class CamClient {
    private static JLabel label;
    private static JFrame window;
    static{
        label = new JLabel();
        label.setBounds(0,0,176,144);
        label.setVisible(true);
        window = new JFrame("Test webcam panel");
        window.add(label);
        window.setLayout(new FlowLayout());
        window.setResizable(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setMinimumSize(new Dimension(176,144));
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) throws Exception {
        args = new String[2];
        args[0] = "20000";
        args[1] = "192.168.1.7";

        int port = 20000;
        String ip = "127.0.0.1";
        if(args.length > 1){
            port = Integer.parseInt(args[0]);
            ip = args[1];
        }
        try {
            Socket dummyClient = new Socket();
            dummyClient.setReuseAddress(true);
            dummyClient.connect(new InetSocketAddress(Inet4Address.getByName(ip), port));
            Runtime.getRuntime().addShutdownHook(new FinalWords(dummyClient));

            DatagramSocket client = new DatagramSocket(dummyClient.getPort(), InetAddress.getByName("0.0.0.0"));

            System.out.println("dummy: " + dummyClient.getInetAddress().toString() + ":" + dummyClient.getPort());
            //byte[] toServerData = ByteBuffer.allocate(4).putInt(1).array();
            byte[] fromServerData = new byte[16384];
            //DatagramPacket toServer;
            DatagramPacket fromServer = new DatagramPacket(fromServerData, fromServerData.length);

            BufferedImage img;
            InputStream byteInputStream;

            //toServer = new DatagramPacket(toServerData, toServerData.length, Inet4Address.getByName("127.0.0.1"), 12345);=

            //client.send(toServer);
            while (true) {
                client.receive(fromServer);
                //System.out.println("Got it!");
                byteInputStream = new ByteArrayInputStream(fromServer.getData());
                img = ImageIO.read(byteInputStream);
                label.setIcon(new ImageIcon(img));
                window.pack();
                label.repaint();
            }
        } catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }
}
class FinalWords extends Thread{
    Socket toClose = null;
    public FinalWords(Socket dummy){
        toClose = dummy;
    }

    @Override
    public void run() {
        super.run();
        try {
            toClose.getOutputStream().write(666);
            toClose.close();
            System.out.println("Closed");
        } catch (Exception e){}
    }
}
